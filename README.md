# README #

[WhereToLiveAndNotDie](http://wheretoliveandnotdie.com) is a heat map showing a relative comparison of the most risky places on earth, based on earthquakes, tsunamis and eruptions data provided by NOAA - [http://www.ngdc.noaa.gov](http://www.ngdc.noaa.gov).

### What is this repository for? ###

* Backend written in Javascript over Node.js runtime.
* Frontend developed using the basics of HTML, Javascript, Jquery and CSS.
* Some external libraries as [Async](https://www.npmjs.com/package/async), [csv-parse](https://www.npmjs.com/package/csv-parse), [express](https://www.npmjs.com/package/express) and [SockJS](https://www.npmjs.com/package/csv-parse).
* Version 0.1.0.

### Setup ###

Clone repo and then
```
#!javascript

node server
```
It will run over the port 5005. Then you just type **localhost:5005** in your browser.

### Collaborators ###

* Francisco Gutiérrez [pancho.gutierrez@outlook.com](mailto:pancho.gutierrez@outlook.com)
* Alejandro Baltra [abaltra@gmail.com](mailto:abaltra@gmail.com)