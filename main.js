var csvParse = require('csv-parse');
var fs = require('fs');
var unzip = require('unzip');

var constants = require('./constants.js');
var dataParser = require('./dataParser.js');
var request = require('./request.js');

var fetchCitiesInterval = undefined;
var fetchDisastersData = undefined;

var countriesInfo = new Array();
var earthquakesInfo = new Array();
var tsunamisInfo = new Array();
var eruptionsInfo = new Array();

var startApp = function() {

	fetchCities();
  fetchEarthquakes();
  fetchTsunamis();
  fetchEruptions();
/*
	fetchCitiesInterval = setInterval(function(){
		fetchCities();
	}, 300 * 1000);*/
}

var fetchCities = function() {
	console.log('Fetching cities');

	fs.readFile('./' + constants.COUNTRIES_FILENAME_JSON, 'utf8', function (error, data) {
    	
		var countries = JSON.parse(data);

    	countries.forEach(function (country){
    		
    		var countryInfo = {
    			name: country.name.common,
    			capital: country.capital,
    			latitude: country.latlng[0],
    			longitude: country.latlng[1]
    		}

    		countriesInfo.push(countryInfo);

    	});
    });

/*

	request(constants.URL_GET_CITIES, 'GET', null, {}, function(error, data){

		if (error) {
			console.log('Error fetching cities: ' + error);
			return;
		} 

		console.log("Cities zip file fetched");
		fs.writeFile('./' + constants.CITIES_FILENAME_ZIP, data, function(error) {
           	if (error) {
           		console.log("Error saving zip file: " + error);
           		return;
           	}
           	
           	console.log('Zip file saved');

           	fs.createReadStream('./' + constants.CITIES_FILENAME_ZIP).pipe(unzip.Extract({ path: './' }));
           	console.log('File unzipped');

           	fs.readFile('./' + constants.CITIES_FILENAME_CSV, 'utf8', function (error, data) {
           		
           		var csvColumns = ['countryCode', 'subdivisionCode', 'gnsFd', 'gnsUfi', 'languageCode', 'languageScript', 'name', 'latitude', 'longitude'];
           		var options = {
           			delimiter: ',',
           			rowDelimiter: '\n',
           			columns: csvColumns,
           			escape: '"',
           		}
           		csvParse(data, options, function (error, output) {
           			if (error) {
           				console.log('Error parsing csv data: ' + error);
           				return;
           			}

           			cities = output;
           			console.log('Csv data parsed, ' + cities.length + ' cities saved');

           		});

           	});

        });

	});

*/
}

var fetchEarthquakes = function() {

  request(constants.URL_GET_EARTHQUAKES, 'GET', null, {}, function(error, data){
    
    fs.writeFile('./' + constants.EARTHQUAKES_FILENAME, data, function(error) {

      if (error) {
        console.log('Error fetching earthqueakes data: ' + error);
        return;
      }

      fs.readFile('./' + constants.EARTHQUAKES_FILENAME, 'utf8', function (error, data) {

        var columns = data.split('\n')[0].split('\t');

        var options = {
          delimiter: '\t',
          rowDelimiter: '\n',
          columns: columns
        }
        csvParse(data, options, function (error, output) {
          if (error) {
            console.log('Error parsing csv data: ' + error);
            return;
          }

          output.splice(0,1);

          earthquakesInfo = [];

          for (i = output.length - 1; i >= 0; i--) {
            var data = output[i];

            earthquake = {
              LOCATION_NAME: data.LOCATION_NAME,
              YEAR: data.YEAR,
              EQ_PRIMARY: data.EQ_PRIMARY,
              LATITUDE: data.LATITUDE,
              LONGITUDE: data.LONGITUDE,
              TOTAL_DEATHS: data.TOTAL_DEATHS
            }
            earthquakesInfo.push(earthquake);
          }

          console.log('Earthquakes data parsed, ' + output.length + ' earthqueakes saved');        
        });

      });

    });
  });
}

var fetchTsunamis = function() {

  request(constants.URL_GET_TSUNAMIS, 'GET', null, {}, function(error, data){
    
    fs.writeFile('./' + constants.TSUNAMIS_FILENAME, data, function(error) {

      if (error) {
        console.log('Error fetching earthqueakes data: ' + error);
        return;
      }

      fs.readFile('./' + constants.TSUNAMIS_FILENAME, 'utf8', function (error, data) {

        var columns = data.split('\n')[0].split('\t');

        var options = {
          delimiter: '\t',
          rowDelimiter: '\n',
          columns: columns
        }
        csvParse(data, options, function (error, output) {
          if (error) {
            console.log('Error parsing csv data: ' + error);
            return;
          }

          output.splice(0,1);

          tsunamisInfo = [];

          for (i = output.length - 1; i >= 0; i--) {
            var data = output[i];

            tsunami = {
              LOCATION_NAME: data.LOCATION_NAME,
              YEAR: data.YEAR,
              EQ_PRIMARY: data.EQ_PRIMARY,
              LATITUDE: data.LATITUDE,
              LONGITUDE: data.LONGITUDE,
              TOTAL_DEATHS: data.TOTAL_DEATHS
            }
            tsunamisInfo.push(tsunami);
          }
          
          console.log('Tsunamis data parsed, ' + output.length + ' tsunamis saved');
        });

      });

    });
  });
}

var fetchEruptions = function() {

  request(constants.URL_GET_ERUPTIONS, 'GET', null, {}, function(error, data){
    
    fs.writeFile('./' + constants.ERUPTIONS_FILENAME, data, function(error) {

      if (error) {
        console.log('Error fetching earthqueakes data: ' + error);
        return;
      }

      fs.readFile('./' + constants.ERUPTIONS_FILENAME, 'utf8', function (error, data) {

        var columns = data.split('\n')[0].split('\t');

        var options = {
          delimiter: '\t',
          rowDelimiter: '\n',
          columns: columns
        }
        csvParse(data, options, function (error, output) {
          if (error) {
            console.log('Error parsing csv data: ' + error);
            return;
          }

          output.splice(0,1);

          eruptionsInfo = [];

          for (i = output.length - 1; i >= 0; i--) {
            var data = output[i];

            eruption = {
              Location: data.Location,
              Year: data.Year,
              EQ_PRIMARY: data.EQ_PRIMARY,
              Latitude: data.Latitude,
              Longitude: data.Longitude,
              TOTAL_DEATHS: data.TOTAL_DEATHS
            }
            eruptionsInfo.push(eruption);
          }

          console.log('Eruptions data parsed, ' + output.length + ' eruptions saved');
        
        });

      });

    });
  });
}

var getCities = function() {
	return countriesInfo;
}

var getEarthquakes = function() {
  return earthquakesInfo;
}

var getTsunamis = function() {
  return tsunamisInfo;
}

var getEruptions = function() {
  return eruptionsInfo;
}

exports.startApp = startApp;

exports.getCities = getCities;
exports.getEarthquakes = getEarthquakes;
exports.getTsunamis = getTsunamis;
exports.getEruptions = getEruptions;