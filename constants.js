exports.APP_IP = '0.0.0.0';
exports.APP_PORT = 5005;

exports.WEB_ROOT = 'public';

exports.URL_GET_CITIES = 'http://www.opengeocode.org/download/worldcities.zip';
exports.URL_GET_EARTHQUAKES = 'http://www.ngdc.noaa.gov/nndc/struts/results?type_0=Exact&query_0=$ID&t=101650&s=13&d=189&dfn=signif.txt';
exports.URL_GET_TSUNAMIS = 'http://www.ngdc.noaa.gov/nndc/struts/results?type_0=Exact&query_0=$ID&t=101650&s=69&d=59&dfn=tsevent.txt';
exports.URL_GET_ERUPTIONS = 'http://www.ngdc.noaa.gov/nndc/struts/results?type_0=Exact&query_0=$HAZ_EVENT_ID&t=102557&s=50&d=54&dfn=volerup.txt';

exports.CITIES_FILENAME_ZIP = 'worldcities.zip';
exports.CITIES_FILENAME_CSV = 'worldcities.csv';

exports.EARTHQUAKES_FILENAME = 'earthquakes.txt';
exports.TSUNAMIS_FILENAME = 'tsunamis.txt';
exports.ERUPTIONS_FILENAME = 'eruptions.txt';

exports.COUNTRIES_FILENAME_JSON = 'countries.json';

exports.MAP_BOX_TOKEN = 'pk.eyJ1IjoicGFudHJ1bG8iLCJhIjoiY2loNWtkZmQwMDA2d3Vnajl3d3c0d2tyMiJ9.fqJQ4zxM76SI6-hph03xew';