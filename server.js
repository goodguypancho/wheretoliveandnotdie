var express = require('express');
var http = require('http');
var sockjs = require('sockjs');

var constants = require('./constants.js');
var main = require('./main.js');

var webApp = express();
var server = http.createServer(webApp);
var port = process.env.port || constants.APP_PORT;

webApp.set('view engine', 'html');
webApp.use(express.static(constants.WEB_ROOT));

server.listen(port, constants.APP_IP);

var socket = sockjs.createServer();
socket.on('connection', function(conn) {

	conn.on('data', function(message){
		var json = JSON.parse(message);

		switch (json.operation) {
			case 'getMapToken':
				getMapToken(conn);
				break;
			case 'getCities':
				getCities(conn);
				break;
			case 'getEarthquakes':
				getEarthquakes(conn);
				break;
			case 'getTsunamis':
				getTsunamis(conn);
				break;
			case 'getEruptions':
				getEruptions(conn);
				break;
		}
	});

});
socket.installHandlers(server, {
  prefix: '/socket'
});

main.startApp();

var getCities = function(conn) {
	var message = {
		operation: 'getCities',
		cities: main.getCities()
	};
	conn.write(JSON.stringify(message));
	//conn.close();
}

var getEarthquakes = function(conn) {
	var message = {
		operation: 'getEarthquakes',
		earthquakes: main.getEarthquakes()
	};
	conn.write(JSON.stringify(message));
	//conn.close();
}

var getTsunamis = function(conn) {
	var message = {
		operation: 'getTsunamis',
		tsunamis: main.getTsunamis()
	};
	conn.write(JSON.stringify(message));
	//conn.close();
}

var getEruptions = function(conn) {
	var message = {
		operation: 'getEruptions',
		eruptions: main.getEruptions()
	};
	conn.write(JSON.stringify(message));
	//conn.close();
}
