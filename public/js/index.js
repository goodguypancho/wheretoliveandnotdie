var sock = undefined;

var allEvents = [];

var earthquakesLatLng = [];
var tsunamisLatLng = [];
var eruptionsLatLng = [];
var allEventsLatLng = [];

var map = undefined;
var earthquakesHeatLayer = undefined;
var tsunamisHeatLayer = undefined;
var eruptionsHeatLayer = undefined;
var allEventsHeatLayer = undefined;

var maxEarthquakeDeaths, maxTsunamiDeaths, maxEruptionDeaths, maxEventDeaths;

var loadingInterval;

var START_YEAR = 1000;

var FETCHING_DATA_MESSAGE_0 = 'Fetching data';
var FETCHING_DATA_MESSAGE_1 = 'Fetching data.';
var FETCHING_DATA_MESSAGE_2 = 'Fetching data..';
var FETCHING_DATA_MESSAGE_3 = 'Fetching data...';

var HEAT_RED = '#D50000';
var HEAT_YELLOW = '#FFFF00';
var HEAT_BLUE = '#651FFF';

var connectToServer = function() {
	 sock = new SockJS(URL_SERVER + SOCKET_ENDPOINT);

	 sock.onopen = function() {
     	//fetchCities();

     	fetchEarthquakes();
     	fetchTsunamis();
     	fetchEruptions();
	 };
	 sock.onmessage = function(e) {
	     var message = JSON.parse(e.data);

	     //console.log("Message operation: " + message.operation);

	     switch (message.operation) {
	     	case 'getCities':
	     		processCities(message.cities);
	     		break;
	     	case 'getEarthquakes':
	     		processEarthquakes(message.earthquakes);
	     		break;
	     	case 'getTsunamis':
	     		processTsunamis(message.tsunamis);
	     		break;
	     	case 'getEruptions':
	     		processEruptions(message.eruptions);
	     		break;
	     }
	 };
	 sock.onclose = function() {
	     console.log('close');
	 };
}

var fetchCities = function() {
	var message = {
		operation: 'getCities'
	};
	sock.send(JSON.stringify(message));
}

var fetchEarthquakes = function() {
	var message = {
		operation: 'getEarthquakes'
	};
	sock.send(JSON.stringify(message));
}

var fetchTsunamis = function() {
	var message = {
		operation: 'getTsunamis'
	};
	sock.send(JSON.stringify(message));
}

var fetchEruptions = function() {
	var message = {
		operation: 'getEruptions'
	};
	sock.send(JSON.stringify(message));
}

var processCities = function(cities) {

	if (!cities) return;

	async.each(cities,
		function (city, cb) {
			createCityMarker(city);
			cb();
		},
		function (error, result) {

			
		}
	);
}

var processEarthquakes = function(earthquakes) {
	if (!earthquakes) return;

	maxEarthquakeDeaths = getMaxEarthquakeDeaths(earthquakes);

	async.each(earthquakes,
		function (earthquake, cb) {
			//console.log(earthquake);
			createEarthquakeMarker(earthquake);
			cb();
		},
		function (error, result) {
			if (error) console.log(error);
			else {
				//earthquakesHeatLayer = L.heatLayer(earthquakesLatLng, { radius: 30, maxZoom: 12, useLocalExtrema: true, blur: 5, gradient: {0.2: 'brown', 0.3: 'yellow', 1: 'red'} }).addTo(map);
				earthquakesHeatLayer = L.heatLayer(earthquakesLatLng, { radius: 20, maxZoom: 12, useLocalExtrema: true, blur: 10, gradient: {0.2: HEAT_BLUE, 0.3: HEAT_YELLOW, 0.7: HEAT_RED} });
				if (areAllHeatLayersReady()) {
					processAllEvents();
				}
			}

		}
	);

}

var processTsunamis = function(tsunamis) {
	if (!tsunamis) return;

	maxTsunamiDeaths = getMaxTsunamiDeaths(tsunamis);

	async.each(tsunamis,
		function (tsunami, cb) {
			//console.log(tsunami);
			createTsunamiMarker(tsunami);
			cb();
		},
		function (error, result) {
			if (error) console.log(error);
			else {
				//tsunamisHeatLayer = L.heatLayer(tsunamisLatLng, { radius: 100, maxZoom: 12, useLocalExtrema: true, blur: 5, gradient: {0.2: 'blue', 0.3: 'yellow', 1: 'red'} }).addTo(map);
				tsunamisHeatLayer = L.heatLayer(tsunamisLatLng, { radius: 20, maxZoom: 12, useLocalExtrema: true, blur: 10, gradient: {0.2: HEAT_BLUE, 0.3: HEAT_YELLOW, 0.7: HEAT_RED} });
				if (areAllHeatLayersReady()) {
					processAllEvents();
				}
			}

		}
	);
}

var processEruptions = function(eruptions) {
	if (!eruptions) return;

	maxEruptionDeaths = getMaxEruptionDeaths(eruptions);

	async.each(eruptions,
		function (eruption, cb) {
			//console.log(tsunami);
			createEruptionMarker(eruption);
			cb();
		},
		function (error, result) {
			if (error) console.log(error);
			else {
				//eruptionsHeatLayer = L.heatLayer(eruptionsLatLng, { radius: 30, maxZoom: 12, useLocalExtrema: true, blur: 1, gradient: {0.2: 'red', 0.3: 'orange', 1: 'red'} }).addTo(map);
				eruptionsHeatLayer = L.heatLayer(eruptionsLatLng, { radius: 20, maxZoom: 12, useLocalExtrema: true, blur: 10, gradient: {0.2: HEAT_BLUE, 0.3: HEAT_YELLOW, 0.7: HEAT_RED} });
				if (areAllHeatLayersReady()) {
					processAllEvents();
				}
			}

		}
	);
}

var processAllEvents = function() {

	maxEventDeaths = getMaxEventDeaths(allEvents);

	async.each(allEvents,
		function (event, cb) {
			createEventMarker(event);
			cb();
		},
		function (error, result) {
			if (error) console.log(error);
			else {
				allEventsHeatLayer = L.heatLayer(allEventsLatLng, { radius: 20, maxZoom: 12, useLocalExtrema: true, blur: 10, gradient: {0.2: HEAT_BLUE, 0.3: HEAT_YELLOW, 0.7: HEAT_RED} });
			}

			clearInterval(loadingInterval);

			$('#loading').html('Data ready, click me!');

			$('#loading').click(function() {
				$('#explanation').css('left', '-1000');

				$('#title').css('transform', 'rotate(17deg)');
				$('#title').css('margin-right', '-800px');

				setTimeout(function(){
					$('#loading').css('top', '200%');
				}, 500);

				setTimeout(function(){
					$('#explanation').css('display', 'none');
					$('#dash').css('left', '100%');

					setTimeout(function(){
						selectLayer('earthquakes');

						$('#dashContent').css('display', 'none');

					}, 2000);

				}, 1000);

				$('#loading').click(function() {});
				
			});
		}
	);
}

var loadMap = function() {
	L.mapbox.accessToken = MAP_BOX_TOKEN;
	map = L.mapbox.map('map', 'pantrulo.nkmj9l9l', {
		center: new L.LatLng(START_LAT, START_LNG),
		zoom: 2,
		zoomControl: false,
	});
}

var createCityMarker = function(city){
	var name = city.name;
	var latitude = city.latitude;
	var longitude = city.longitude;

	try {
		var marker = L.mapbox.featureLayer({
		    type: 'Feature',
		    geometry: {
		        type: 'Point',
		        coordinates: [longitude, latitude]
		    },
		    properties: {
		        title: name,
		        'marker-size': 'small',
        		'marker-color': '#BE9A6B',
        		'marker-symbol': 'star'
	    	}
		}).addTo(map);

	} catch (e) {
		console.log(e);
	}
}

var createEarthquakeMarker = function(earthquake){

	var name = earthquake.LOCATION_NAME;
	var year = earthquake.YEAR;
	var magnitude = earthquake.EQ_PRIMARY;
	var latitude = parseFloat(earthquake.LATITUDE);
	var longitude = parseFloat(earthquake.LONGITUDE);

	if (latitude && longitude) {
		// Check relative deadliness
		var totalDeaths = earthquake.TOTAL_DEATHS;
		var deadliness = 0;

		if (totalDeaths && year > START_YEAR) {
			deadliness = Math.ceil((totalDeaths/maxEarthquakeDeaths)*5000);
		}

		// Sum up that location many times as deadly the event was
		if (deadliness == 0) earthquakesLatLng.push([latitude, longitude]);
		for (var i = 0; i < deadliness; i++) {
			earthquakesLatLng.push([latitude, longitude]);
		}

		allEvents.push(earthquake);
	}


/*
	// This creates a real marker for the event, but with so many events it's really slow
	try {
		var marker = L.mapbox.featureLayer({
		    type: 'Feature',
		    geometry: {
		        type: 'Point',
		        coordinates: [longitude, latitude]
		    },
		    properties: {
		        title: name + ', year ' + year + ', magnitude ' + magnitude,
		        'marker-size': 'small',
        		'marker-color': '#BE9A6B',
        		'marker-symbol': 'star'
	    	}
		}).addTo(map);
		console.log(marker)

	} catch (e) {
		console.log(e);
	} */
}

var createTsunamiMarker = function(tsunami){
	var name = tsunami.LOCATION_NAME;
	var year = parseInt(tsunami.YEAR);
	var magnitude = tsunami.EQ_PRIMARY;
	var latitude = parseFloat(tsunami.LATITUDE);
	var longitude = parseFloat(tsunami.LONGITUDE);

	if (latitude && longitude) {
		// Check relative deadliness
		var totalDeaths = parseInt(tsunami.TOTAL_DEATHS);
		var deadliness = 0;

		if (totalDeaths && year > START_YEAR) {
			deadliness = Math.ceil((totalDeaths/maxTsunamiDeaths)*5000);
		}

		// Sum up that location many times as deadly the event was
		if (deadliness == 0) tsunamisLatLng.push([latitude, longitude]);
		for (var i = 0; i < deadliness; i++) {
			tsunamisLatLng.push([latitude, longitude]);
		}

		allEvents.push(tsunami);
	}
}

var createEruptionMarker = function(eruption){
	var name = eruption.Location;
	var year = eruption.Year;
	var magnitude = eruption.EQ_PRIMARY;
	var latitude = parseFloat(eruption.Latitude);
	var longitude = parseFloat(eruption.Longitude);

	if (latitude && longitude) {
		// Check relative deadliness
		var totalDeaths = eruption.TOTAL_DEATHS;
		var deadliness = 0;

		if (totalDeaths && year > START_YEAR) {
			deadliness = Math.ceil((totalDeaths/maxEruptionDeaths)*5000);
		}

		// Sum up that location many times as deadly the event was
		if (deadliness == 0) eruptionsLatLng.push([latitude, longitude]);
		for (var i = 0; i < deadliness; i++) {
			eruptionsLatLng.push([latitude, longitude]);
		}

		allEvents.push(eruption);
	}
}

var createEventMarker = function(event, type) {
	var name = event.Location || event.LOCATION_NAME;
	var year = event.Year || event.YEAR;
	var magnitude = event.EQ_PRIMARY;
	var latitude = parseFloat(event.Latitude || event.LATITUDE);
	var longitude = parseFloat(event.Longitude || event.LONGITUDE);

	if (latitude && longitude) {
		// Check relative deadliness
		var totalDeaths = event.TOTAL_DEATHS;
		var deadliness = 0;

		if (totalDeaths && year > START_YEAR) {
			deadliness = Math.ceil((totalDeaths/maxEventDeaths)*5000);
		}

		// Sum up that location many times as deadly the event was
		if (deadliness == 0) allEventsLatLng.push([latitude, longitude]);
		for (var i = 0; i < deadliness; i++) {
			allEventsLatLng.push([latitude, longitude]);
		}
	}
}

var selectLayer = function(selector) {

	switch (selector) {
		case 'earthquakes':
			earthquakesHeatLayer.addTo(map);
			map.removeLayer(tsunamisHeatLayer);
			map.removeLayer(eruptionsHeatLayer);
			map.removeLayer(allEventsHeatLayer);
			break;

		case 'tsunamis':
			tsunamisHeatLayer.addTo(map);
			map.removeLayer(earthquakesHeatLayer);
			map.removeLayer(eruptionsHeatLayer);
			map.removeLayer(allEventsHeatLayer);
			break;

		case 'eruptions':
			eruptionsHeatLayer.addTo(map);
			map.removeLayer(earthquakesHeatLayer);
			map.removeLayer(tsunamisHeatLayer);
			map.removeLayer(allEventsHeatLayer);
			break;
		case 'all':
			map.removeLayer(eruptionsHeatLayer);
			map.removeLayer(earthquakesHeatLayer);
			map.removeLayer(tsunamisHeatLayer);
			allEventsHeatLayer.addTo(map);
			break;
	}

	colorButtons(selector);
}

var colorButtons = function(selector) {

	var buttons = {
		earthquakes: $('#earthquakesButton'),
		tsunamis: $('#tsunamisButton'),
		eruptions: $('#eruptionsButton'),
		all: $('#allButton')
	};

	for (key in buttons) {
		if (selector == key) buttons[key].css('background-color', 'rgba(67,160,71, 0.5)');
		else buttons[key].css('background-color', 'rgba(300, 300, 300, 0.5)');
	}
}

var printMinMax = function(numberArray) {
	var min, max;

	numberArray.forEach(function(number){
		if (!min) min = number;
		if (!max) max = number;

		if (number > max) max = number;
		if (number < min) min = number;
	});

}

var getMaxEarthquakeDeaths = function(earthquakes) {
	var deaths = new Array();

	earthquakes.forEach(function(earthquake){
		deaths.push(earthquake.TOTAL_DEATHS);
	});

	return max(deaths);
}

var getMaxTsunamiDeaths = function(tsunamis) {
	var deaths = new Array();

	tsunamis.forEach(function(tsunami){
		deaths.push(tsunami.TOTAL_DEATHS);
	});

	return max(deaths);
}

var getMaxEruptionDeaths = function(eruptions) {
	var deaths = new Array();

	eruptions.forEach(function(eruption){
		deaths.push(eruption.TOTAL_DEATHS);
	});

	return max(deaths);
}

var getMaxEventDeaths = function(events) {
	var deaths = new Array();

	events.forEach(function(event){
		deaths.push(event.TOTAL_DEATHS);
	});

	return max(deaths);
}

var max = function(array) {
	var max;

	array.forEach(function(number){
		number = parseInt(number);
		if (!max) max = number;
		if (number > max) max = number;
	});
	return max;
}

var areAllHeatLayersReady = function() {
	if (earthquakesHeatLayer && tsunamisHeatLayer && eruptionsHeatLayer) return true;
	return false;
}

var animateLoading = function() {
	loadingInterval = setInterval(function(){
		var loading = $('#loading');
		var text = loading.html();

		switch(text) {
			case FETCHING_DATA_MESSAGE_0:
				loading.html(FETCHING_DATA_MESSAGE_1);
				break;
			case FETCHING_DATA_MESSAGE_1:
				loading.html(FETCHING_DATA_MESSAGE_2);
				break;
			case FETCHING_DATA_MESSAGE_2:
				loading.html(FETCHING_DATA_MESSAGE_3);
				break;
			case FETCHING_DATA_MESSAGE_3:
				loading.html(FETCHING_DATA_MESSAGE_0);
				break;
		}
	}, 500);
}

$(document).ready(function(){
	animateLoading();
	connectToServer();
	loadMap();
});